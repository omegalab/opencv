FROM nvidia/cuda:11.4.1-cudnn8-devel-ubuntu20.04 AS base

ENV NVIDIA_DRIVER_CAPABILITIES=compute,video,utility

RUN rm -f /usr/local/cuda-11.4/cuda-11.4 && ln -s /etc/alternatives/cuda /usr/local/nvidia

FROM base AS build

ARG OPENCV_VERSION=4.5.3
ARG FFMPEG_VERSION=4.4
ARG GST_VERSION=1.16.2
ARG OPENH264_VERSION=v2.1.1

ADD nv-video-codec-sdk /usr/local/cuda/

WORKDIR /build

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends build-essential curl cmake git yasm \
    gettext  libtool autoconf autopoint automake libgstreamer-plugins-base1.0-dev libgstreamer1.0-dev gstreamer1.0-tools \
    libfreetype6-dev libfreetype6 unzip nasm libharfbuzz-bin libharfbuzz-dev libvpx-dev openssl libgcrypt20-dev \
    libnuma-dev libopenblas-dev liblapacke-dev libdc1394-dev python3-dev libeigen3-dev python3-numpy && \
    ln -s /usr/include/lapacke.h /usr/include/x86_64-linux-gnu && \
    git clone https://github.com/FFmpeg/nv-codec-headers.git && cd nv-codec-headers && \
    make && make install && cd /build && \
    git clone https://code.videolan.org/videolan/x264.git --depth=1 && \
    cd x264 && ./configure --prefix=/usr/local/libx264 --disable-cli --enable-static --enable-shared && \
    make -j$(nproc) && make install && export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/libx264/lib/pkgconfig && \
    echo "/usr/local/libx264/lib" > /etc/ld.so.conf.d/x-codecs.conf && ldconfig && cd .. && \
    git clone https://github.com/videolan/x265.git && \
    cd x265/build/linux && cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local/libx265 ../../source/ && \
    make -j$(nproc) && make install &&  export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/libx265/lib/pkgconfig && \
    echo "/usr/local/libx265/lib" >> /etc/ld.so.conf.d/x-codecs.conf && ldconfig && cd /build && \
    curl -s https://ffmpeg.org/releases/ffmpeg-${FFMPEG_VERSION}.tar.bz2 | tar xj && cd ffmpeg-${FFMPEG_VERSION} && \
    sed -i 's/arch=compute_30,code=sm_30/arch=compute_50,code=sm_50/g' configure && \
    sed -i 's/--cuda-gpu-arch=sm_30/--cuda-gpu-arch=sm_50/g' configure && \
    ./configure --enable-cuda-nvcc --enable-cuda --enable-cuvid --enable-nvenc --enable-nonfree --disable-debug --enable-libnpp \
    --enable-avresample --enable-swresample --enable-libx264 --enable-libx265 --enable-gpl --extra-cflags=-I/usr/local/cuda/include \
    --extra-ldflags=-L/usr/local/cuda/lib64 --enable-shared --prefix=/usr/local/ffmpeg  && \
    make -j$(nproc) && make install && export PKG_CONFIG_PATH=$PKG_CONFIG_PATH:/usr/local/ffmpeg/lib/pkgconfig && \
    echo "/usr/local/ffmpeg/lib" > /etc/ld.so.conf.d/ffmpeg.conf && ldconfig && cd /build && \
    git clone -b ${OPENH264_VERSION} https://github.com/cisco/openh264.git --depth=1 && cd openh264 && \
    make -j$(nproc) && make install && cd /build && \
    git clone -b ${GST_VERSION} https://gitlab.freedesktop.org/gstreamer/gst-plugins-bad.git && cd gst-plugins-bad && \
    ./autogen.sh LDFLAGS="-L/usr/local/cuda/lib64" --prefix=/usr/local/gstreamer --disable-gtk-doc --enable-openh264 --enable-x265 --enable-cuda \
    --with-cuda-prefix=/usr/local/cuda --enable-nvdec --enable-nvenc --enable-shared --disable-examples --disable-rpath && \
    make -j$(nproc) && make install
RUN git clone -b ${OPENCV_VERSION} https://github.com/opencv/opencv.git --depth=1 && \
    git clone -b ${OPENCV_VERSION} https://github.com/opencv/opencv_contrib.git --depth=1 && \
    mkdir opencv_build && cd opencv_build && \
    cmake \
    -D CMAKE_BUILD_TYPE=RELEASE \
    -D CMAKE_INSTALL_PREFIX=/usr/local/opencv \
    -D OPENCV_EXTRA_MODULES_PATH=../opencv_contrib/modules \
    -D EIGEN_INCLUDE_PATH=/usr/include/eigen3 \
    -D OPENCV_ENABLE_NONFREE=OFF \
    -D OPENCV_GENERATE_PKGCONFIG=ON \
    -D BUILD_EXAMPLES=OFF \
    -D BUILD_DOCS=OFF \
    -D BUILD_OPENCV_LEGACY=OFF \
    -D BUILD_TESTS=OFF \
    -D BUILD_PERF_TESTS=OFF \
    -D BUILD_OPENCV_PYTHON2=OFF \
    -D BUILD_OPENCV_PYTHON3=ON \
    -D WITH_CUDA=ON \
    -D CUDA_ARCH_BIN="6.1 7.0 7.5" \
    -D ENABLE_FAST_MATH=ON \
    -D CUDA_FAST_MATH=ON \
    -D WITH_CUBLAS=ON \
    -D WITH_TBB=OFF \
    -D OPENCV_DNN_CUDA=ON \
    -D WITH_NVCUVID=OFF \
    -D WITH_OPENCL=OFF \
    -D WITH_GSTREAMER=ON \
    -D VIDEOIO_PLUGIN_LIST=gstreamer,ffmpeg \
    ../opencv && \
    make -j$(nproc) && make install

FROM base

ENV TZ=Europe/Moscow \
    GST_PLUGIN_PATH=/usr/local/gstreamer/lib/gstreamer-1.0 \
    PATH=$PATH:/usr/local/opencv/bin:/usr/local/ffmpeg/bin \
    LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/ffmpeg/lib \
    PYTHONPATH=/usr/local/opencv/lib/python3.8/dist-packages

COPY --from=build /usr/local/gstreamer /usr/local/gstreamer
COPY --from=build /usr/local/include /usr/local/include
COPY --from=build /usr/local/lib /usr/local/lib
COPY --from=build /usr/local/libx264 /usr/local/libx264
COPY --from=build /usr/local/libx265 /usr/local/libx265
COPY --from=build /usr/local/ffmpeg /usr/local/ffmpeg
COPY --from=build /usr/local/opencv /usr/local/opencv

RUN apt-get update && DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    curl openssl libgcrypt20 libfreetype6 libharfbuzz-bin python3-numpy \
    libnuma1 libvpx6 libxcb-shape0 libopenblas-base libdc1394-25 libraw1394-11 libraw1394-tools libusb-1.0-0 \
    gstreamer1.0-rtsp gstreamer1.0-plugins-base-apps gstreamer1.0-plugins-base \
    gstreamer1.0-plugins-good gstreamer1.0-plugins-bad gstreamer1.0-gl gstreamer1.0-tools && \
    echo "/usr/local/libx264/lib" > /etc/ld.so.conf.d/x-codecs.conf && \
    echo "/usr/local/libx265/lib" >> /etc/ld.so.conf.d/x-codecs.conf && \
    echo "/usr/local/ffmpeg/lib" > /etc/ld.so.conf.d/ffmpeg.conf && \
    echo "/usr/local/opencv/lib" > /etc/ld.so.conf.d/opencv.conf && \
    ldconfig  && apt-get clean all && rm -rf /tmp/*
